package edu.ntnu.stud.launch;

import edu.ntnu.stud.ui.UserInterface;

/**
 * Class for the static main method of the program that runs the application. TrainDispatchApp - TDA
 *
 * @author 111770
 * @version 1.0
 * @since 0.3
 */
public class TrainDispatchApp {

  /**
   * The main method of the program.
   *
   * @param args is the arguments of the main method
   */
  public static void main(String[] args) {


    UserInterface userInterface = new UserInterface();

    userInterface.init();
    userInterface.start();


  }
}
