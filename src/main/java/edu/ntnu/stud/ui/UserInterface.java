package edu.ntnu.stud.ui;

import edu.ntnu.stud.model.Departure;
import edu.ntnu.stud.model.TrainDepartureRegister;
import edu.ntnu.stud.utils.UiValidators;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * This class is the user-controlled menu for the train station. The class has the start og init
 * methods.
 *
 * @author 111770
 * @version 1.0
 * @since 0.3
 */
public class UserInterface {

  TrainDepartureRegister osloCentralStation;

  private static final int END_PROGRAM = 1;

  private static final int ADD_DEPARTURE = 2;

  private static final int REMOVE_DEPARTURE = 3;

  private static final int SHOW_ALL_DEPARTURES = 4;

  private static final int SHOW_DEPARTURES_THROUGH_DESTINATION = 5;

  private static final int SHOW_DEPARTURE_THROUGH_TRAIN_NUMBER = 6;

  private static final int SET_DELAY_THROUGH_TRAIN_NUMBER = 7;

  private static final int ASSIGN_TRACK_THROUGH_TRAIN_NUMBER = 8;

  private static final int SET_THE_TIME = 9;

  private static final int SHOW_THE_TIME = 10;

  private static final int STATISTICS_MODULE = 11;

  String edge =
      "------------------------------------"
          + "-----------------------------------------------------------------------";

  String title =
      "| Time of Departure | Line | Train Number |          "
          + "Destination         | New Time of Departure | Track  |";

  Scanner scanner = new Scanner(System.in);

  /**
   * The main method of the program. This is where the program starts with some departures already
   * initialized
   */
  public void start() {

    osloCentralStation.addNewDeparture(14, 20, "L1", 5, "Bergen", 2, 45);

    osloCentralStation.addNewDeparture(15, 20, "L2", 8, "Bergen", 2, 3);

    osloCentralStation.addNewDeparture(17, 20, "L3", 9, "Oslo", -1, 3);

    osloCentralStation.addNewDeparture(20, 20, "L4", 10, "Stavanger", 5, 0);

    /*
    Runs the main menu by printing the main menu options. Interprets the user input against a
    switch-case to perform the user-requested action.

    If the input is not a whole number, the program will ask the user to type a whole number
    if the input is not within the range of 1-11,
    the program will ask the user to type a number between 1 and 11

     */
    boolean value = true;

    while (value) {

      showMenu();

      try {
        int answer = scanner.nextInt();

        switch (answer) {
          case END_PROGRAM -> {
            System.out.println("The program is now terminated");
            value = false;
          }
          case ADD_DEPARTURE -> addDeparture();
          case REMOVE_DEPARTURE -> removeDeparture();
          case SHOW_ALL_DEPARTURES -> showAllDepartures();
          case SHOW_DEPARTURES_THROUGH_DESTINATION -> showDeparturesThroughDestination();
          case SHOW_DEPARTURE_THROUGH_TRAIN_NUMBER -> showDepartureThroughTrainNumber();
          case SET_DELAY_THROUGH_TRAIN_NUMBER -> addDelayThroughTrainNumber();
          case ASSIGN_TRACK_THROUGH_TRAIN_NUMBER -> assignTrackThroughTrainNumber();
          case SET_THE_TIME -> setTheTime();
          case SHOW_THE_TIME -> showTheTime();
          case STATISTICS_MODULE -> statisticsModule();
          default -> System.out.println("You need to type a number between 1 and 11");
        }
      } catch (java.util.InputMismatchException e) {
        System.out.println("You need to type a whole number");
        scanner.nextLine();
      }

      // updates the current departures in accordance with the current time
      osloCentralStation.removeExpiredDepartures();
    }
  }

  /**
   * The init method of the program. This is where the object of the TrainDepartureRegister class is
   * initialized
   */
  public void init() {

    // the current time of the day is set to 0 hours and 0 minutes so that all the objects in the
    // start method will be shown to the user
    int theCurrentTimeHours = 0;
    int theCurrentTimeMinutes = 0;

    // the internal clock of the program is set to these aforementioned hours and minutes
    osloCentralStation = new TrainDepartureRegister(theCurrentTimeHours, theCurrentTimeMinutes);
  }

  /** A method to show the main menu of the program. */
  private void showMenu() {
    System.out.println("This is the main module of the program");
    System.out.println("1 - to end the program");
    System.out.println("2 - to add a new departure");
    System.out.println("3 - to remove a departure");
    System.out.println("4 - to show all departures");
    System.out.println("5 - to show all departures to a specific destination");
    System.out.println("6 - to show a departure ");
    System.out.println("7 - to set a delay on a departure ");
    System.out.println("8 - to assign a departure to a track ");
    System.out.println("9 - to change the current time");
    System.out.println("10 - to show the current time");
    System.out.println("11 - Statistics Module");
  }

  /**
   * Adds a departure object to the TrainDepartureRegister objects arraylist if certain conditions
   * are not met, the departure will not be added.
   */
  public void addDeparture() {
    System.out.println("Type the information for the train departure here");

    System.out.println("What is the time of Departure HH (0-23)");
    int timeOfDepartureHh = UiValidators.getIntInput(scanner);
    if (timeOfDepartureHh == -2) {
      return;
    }

    System.out.println("What is the time of Departure MM (0-59)");
    int timeOfDepartureMm = UiValidators.getIntInput(scanner);
    scanner.nextLine();

    System.out.println("What is the line?");
    String line = UiValidators.readValidLine(scanner);
    if (line.isEmpty()) {
      return;
    }

    System.out.println("What is the train number? (1-9999)");
    final int trainNumber = UiValidators.getIntInput(scanner);
    if (trainNumber == -2) {
      return;
    }
    scanner.nextLine();

    System.out.println("What is the destination?");
    String destination = UiValidators.readValidDestination(scanner);
    if (destination.isEmpty()) {
      return;
    }

    /* ask if the departure has an assigned track, if not yes or YES assign track, if not the
    track is assigned the value -1 to represent a departure not being assigned a track */
    System.out.println("Does the departure already have an assigned track?");
    System.out.println(
        "type yes if so, if no type a random input and "
            + " the departure will automatically be assigned"
            + " as a departure without an assigned track");
    String assignmentAnswer = scanner.nextLine();

    if (assignmentAnswer.equalsIgnoreCase("yes")) {
      System.out.println("Which track does the train depart from?");
      System.out.println(
          "The track needs to be a positive integer,"
              + " or else the departure will not be assigned a track");
      int theTrack = UiValidators.getIntInput(scanner);

      scanner.nextLine();

      try {
        osloCentralStation.addNewDeparture(
            timeOfDepartureHh,
            timeOfDepartureMm,
            line,
            trainNumber,
            destination.trim(),
            theTrack,
            0);
      } catch (IllegalArgumentException e) {
        System.out.println(e.getMessage());
      }

    } else {

      int theTrack = -1;

      try {
        osloCentralStation.addNewDeparture(
            timeOfDepartureHh,
            timeOfDepartureMm,
            line,
            trainNumber,
            destination.trim(),
            theTrack,
            0);
      } catch (IllegalArgumentException e) {
        System.out.println(e.getMessage());
      }
    }
  }

  /** Shows all departures in the program. */
  private void showAllDepartures() {

    if (osloCentralStation.deepCopy().isEmpty()) {
      System.out.println("There are no departures");
      return;
    }
    tableHeader();
    System.out.println(osloCentralStation.toString());
    tableFooter();
  }

  /** Shows all departures to a specific destination. */
  private void showDeparturesThroughDestination() {

    System.out.println("Info about departure(s) through a destination");
    System.out.println(
        "Which destination do you want to see the departure info of? Type the destination: ");

    scanner.nextLine();
    String whereTheTrainShall = scanner.nextLine();

    ArrayList<Departure> result =
        osloCentralStation.showDeparturesThroughDestination(whereTheTrainShall.trim());

    if (result.isEmpty()) {
      System.out.println("There are no departures to " + whereTheTrainShall);
    } else if (result.size() == 1) {
      System.out.println("The train departure to " + whereTheTrainShall + ":");
      tableHeader();
      result.forEach(System.out::println);
      tableFooter();
    } else {
      System.out.println("The train departures to " + whereTheTrainShall + ":");
      tableHeader();
      result.forEach(System.out::println);
      tableFooter();
    }
  }

  /** Shows a departure of a specific train number. */
  private void showDepartureThroughTrainNumber() {
    System.out.println("Info about a departure through the train number");
    System.out.println(
        "Which departure do you want to see the departure info of? Type the train number: ");

    int theNumber = UiValidators.getIntInput(scanner);
    Departure result = osloCentralStation.showDepartureTroughTrainNumber(theNumber);

    if (result == null && (theNumber < -1)) {
      System.out.println(
          "The train number needs to be a positive integer or -1 " + theNumber + " isn't possible");
    } else if (result == null && (theNumber != -1)) {
      System.out.println("There are no departures with the train number " + theNumber);
    } else {
      System.out.println("Train Departure with the train number " + theNumber + ":");
      tableHeader();
      System.out.println(result);
      tableFooter();
    }
  }

  /** Adds a delay on a departure through a specific train number. */
  private void addDelayThroughTrainNumber() {
    System.out.println("Which departure do you want to add a delay on? Type the train number: ");
    int theTrainNumber = UiValidators.getIntInput(scanner);

    System.out.println("What is the delay on the departure?");
    System.out.println(
        "The delay needs to be a positive integer and needs to be different "
            + "to the current delay or else the delay won't be changed");
    int theDelay = UiValidators.getIntInput(scanner);

    try {
      osloCentralStation.setDelayTroughTrainNumber(theTrainNumber, theDelay);
    } catch (IllegalArgumentException e) {
      System.out.println(e.getMessage());
    }
  }

  /** Assigns a departure to a track through a specific train number. */
  private void assignTrackThroughTrainNumber() {
    System.out.println(
        "Which departure do you want to assign to a specific track?" + " Type the train number: ");
    int theTrainNumber = UiValidators.getIntInput(scanner);

    System.out.println("Which track do you want to assign the departure to?");
    System.out.println("choose between 1-20  or type -1 to unassign a track");
    int theTrack = UiValidators.getIntInput(scanner);
    try {
      osloCentralStation.assignTrackTroughTrainNumber(theTrainNumber, theTrack);
    } catch (IllegalArgumentException e) {
      System.out.println(e.getMessage());
    }
  }

  /** Removes a departure through a specific train number. */
  private void removeDeparture() {
    System.out.println("Which departure do you want to remove? Type the train number: ");
    int theTrainNumber = UiValidators.getIntInput(scanner);

    try {
      osloCentralStation.removeDepartureThroughTrainNumber(theTrainNumber);
    } catch (IllegalArgumentException e) {
      System.out.println(e.getMessage());
    }
  }

  /** Sets the current time of the day. */
  private void setTheTime() {
    System.out.println("Select the time, remember you can't go back in time");

    System.out.println("Hours in HH (0-23): ");
    int inputHours = UiValidators.getIntInput(scanner);

    System.out.println("Minutes in MM (0-59): ");
    int inputMinutes = UiValidators.getIntInput(scanner);

    try {
      osloCentralStation.setTheTime(inputHours, inputMinutes);
    } catch (IllegalArgumentException e) {
      System.out.println(e.getMessage());
    }
  }

  /** Shows the current time of the day. */
  private void showTheTime() {
    LocalTime theTime = osloCentralStation.getCurrentTime();
    System.out.println("The time current is " + theTime);
  }

  /** Prints the table header. */
  private void tableHeader() {
    System.out.println(edge);
    System.out.println(title);
    System.out.println(edge);
  }

  /** Prints the table footer. */
  private void tableFooter() {
    System.out.println(edge);
  }

  private void showMenuStatisticsModule() {
    System.out.println("1 - to return to the main menu");
    System.out.println("2 - to show the average delay of the train departures");
    System.out.println("3 - to show the percentage of train departures on time");
    System.out.println("4 - to show the most popular destination(s)");
    System.out.println("5 - to show the most delayed departure(s)");
  }

  /**
   * The statistics module of the program. This is where the user can see statistics about the
   * departures
   *
   * <p>if the input is not a whole number, the program will ask the user to type a whole number if
   * the input is not within the range of 1-5, the program will ask the user to type an integer
   * within the range
   */
  public void statisticsModule() {
    final int avgDelay = 2;
    final int percentageOnTime = 3;
    final int mostPopularDestination = 4;
    final int mostDelayedDeparture = 5;

    boolean value = true;

    showMenuStatisticsModule();

    while (value) {
      try {
        int answer = scanner.nextInt();

        switch (answer) {
          case END_PROGRAM -> {
            System.out.println("You have now returned to the main menu");
            value = false;
          }
          case avgDelay -> averageDelayOfTrainDepartures();
          case percentageOnTime -> percentageOfTrainDeparturesOnTime();
          case mostPopularDestination -> mostPopularDestinations();
          case mostDelayedDeparture -> mostDelayedDepartures();
          default -> System.out.println("You need to type a number between 1 and 5");
        }
      } catch (java.util.InputMismatchException e) {
        System.out.println("You need to type a whole number");
        scanner.nextLine();
        showMenuStatisticsModule();
      }
    }
  }

  private void averageDelayOfTrainDepartures() {
    try {
      double result = osloCentralStation.averageDelay();

      System.out.println("The average delay of the train departures is: " + result + " minutes");
    } catch (IllegalArgumentException e) {
      System.out.println(e.getMessage());
    }
  }

  private void percentageOfTrainDeparturesOnTime() {

    try {
      double result = osloCentralStation.onTimePercentage();

      System.out.println("The percentage of train departures on time is: " + result + "%");
    } catch (IllegalArgumentException e) {
      System.out.println(e.getMessage());
    }
  }

  private void mostPopularDestinations() {
    try {
      List<String> result = osloCentralStation.mostPopularDestinations();

      if (result.size() == 1) {
        System.out.println("The most popular destination is: " + result.get(0));
      } else {
        System.out.println("The most popular destinations are: " + result);
      }
    } catch (IllegalArgumentException e) {
      System.out.println(e.getMessage());
    }
  }

  private void mostDelayedDepartures() {

    try {
      List<Departure> result = osloCentralStation.getMostDelayedDepartures();

      if (result.size() == 1) {
        System.out.println("The most delayed departure is: " + "\n");
        tableHeader();
        System.out.println(result.get(0));
        tableFooter();
      } else {
        System.out.println("The most delayed departures are: " + "\n");
        tableHeader();
        result.forEach(System.out::println);
        tableFooter();
      }
    } catch (IllegalArgumentException e) {
      System.out.println(e.getMessage());
    }
  }
}
