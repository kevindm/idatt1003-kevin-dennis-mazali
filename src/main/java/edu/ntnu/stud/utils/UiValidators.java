package edu.ntnu.stud.utils;

import edu.ntnu.stud.ui.UserInterface;
import java.util.Scanner;
/**
 * Class containing static methods to validate the input of the user.
 */

public class UiValidators {

  /**
   * Gets input from the user and checks if the input is a whole number, if not, the user will be
   * asked to type a whole number. The user has three chances to type a whole number before the
   * method returns the value -2. To exit from the case method prematurely. {@link
   * UserInterface#addDeparture()}
   *
   * @param scanner is the scanner object used to get the input from the user
   * @return the correct input from the user or -2
   */
  public static int getIntInput(Scanner scanner) {
    int numberOfChances = 3;
    int start = 0;
    while (true) {
      start++;
      try {
        return scanner.nextInt();
      } catch (java.util.InputMismatchException e) {
        System.out.println("You need to type a whole number");
        System.out.println("You have " + (numberOfChances - start) + " chances left");
        scanner.nextLine();
      }
      if (start == 3) {
        return -2;
      }
    }
  }

  /**
   * Checks if the inputted line follows the regex format.
   *
   * @return true or false, depending on if the input the meets the regex requirements
   */
  private static boolean lineIsValid(String line) {
    return line.matches("[A-Z][0-9]") || line.matches("[A-Z][0-9][0-9]?");
  }

  /**
   * Gives the users three chances to type a valid destination before
   * the case method is prematurely.
   * Exited and goes back to the main menu {@link UserInterface#addDeparture()}
   *
   * @param scanner is the scanner object used to get input from the user
   * @return the correct input from the user or ""
   */
  public static String readValidLine(Scanner scanner) {
    String line = null;
    int numberOfChances = 3;

    for (int start = 1; start <= numberOfChances; start++) {
      System.out.println(
          "Type the line in the format of a capitalized letter"
                  + " between A-Z followed by a number between 1-99");
      line = scanner.nextLine().trim();

      if (!line.isBlank() && lineIsValid(line)) {
        return line;
      } else if (start == numberOfChances) {
        System.out.println("You have no more chances left");
        System.out.println("The departure won't be added");
        line = "";
      }
      System.out.println("You have " + (numberOfChances - start) + " chances left");
    }

    return line;
  }

  /**
   * Checks if the inputted destination follows the regex format.
   *
   * @return true or false, depending on if the input the meets the regex requirements
   */
  private static boolean destinationIsValid(String destination) {
    return destination.matches("^[A-Za-zÆØÅæøå -]+$");
  }

  /**
   * Gives the users three chances to type a valid destination before
   * the case method is prematurely.
   * exited and goes back to the main menu {@link UserInterface#addDeparture()}
   *
   * @param scanner is the scanner object used to get input from the user
   * @return the correct input from the user or ""
   */
  public static String readValidDestination(Scanner scanner) {
    String destination = null;
    int numberOfChances = 3;

    for (int start = 1; start <= numberOfChances; start++) {
      System.out.println("Type the destination using only letters");
      destination = scanner.nextLine();

      if (!destination.isBlank() && destinationIsValid(destination)) {
        return destination;
      } else if (start == numberOfChances) {
        System.out.println("You have no more chances left");
        System.out.println("The departure won't be added");
        destination = "";
      }
      System.out.println("You have " + (numberOfChances - start) + " chances left");
    }

    return destination;
  }
}
