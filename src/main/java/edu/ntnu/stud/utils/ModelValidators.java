package edu.ntnu.stud.utils;

/**
 * Class containing static methods to validate the input of the user.
 */
public class ModelValidators {

  /**
   * Checks if the <code>inputHours</code> is between 0 and 23.
   *
   * @param inputHours is an int value representing the number
   *                   of hours of the clock in a HH:MM format
   * @throws IllegalArgumentException if the input hours are not between 0 and 23
   */
  public static void validateHours(int inputHours) {
    if (inputHours < 0 || inputHours > 23) {
      throw new IllegalArgumentException("Hours must be between 0 and 23");
    }
  }

  /**
   * Checks if the <code>inputMinutes</code> are between 0 and 59.
   *
   * @param inputMinutes is an int value the number of minutes of the clock in a HH:MM format
   * @throws IllegalArgumentException if the input minutes are not between 0 and 59
   */
  public static void validateMinutes(int inputMinutes) {
    if (inputMinutes < 0 || inputMinutes > 59) {
      throw new IllegalArgumentException("Minutes must be between 0 and 59");
    }
  }

  /**
   * Checks if the <code>inputTrainNumber</code> is between 1 and 9999.
   *
   *
   * @param inputTrainNumber is an int value representing the unique number each departure gets when
   *     a departure is scheduled
   *
   *@throws IllegalArgumentException if the input train number is not between 1 and 9999
   */
  public static void validateTrainNumber(int inputTrainNumber) {
    if (inputTrainNumber < 1 || inputTrainNumber > 9999) {
      throw new IllegalArgumentException("Train number must be between 1 and 9999");
    }
  }

  /**
   * Checks if the <code>inputDelay</code> is between 0 and 60.
   *
   * @param inputDelay is an int value representing the number of minutes a departure is delayed
   * @throws IllegalArgumentException if the input delay is not between 0 and 60
   */
  public static void validateDelayRange(int inputDelay) {
    if (inputDelay < 0 || inputDelay > 60) {
      throw new IllegalArgumentException("The delay must be between 1 and 60");
    }
  }

  /**
   * Checks if the <code>inputLine</code> is a capitalized letter between
   * A-Z followed by a number between 1-99.
   *
   * @param inputLine is a String to describe which line the departure belongs to
   * @throws IllegalArgumentException if the input line does not match the regex format
   */
  public static void validateLine(String inputLine) {
    if (!(inputLine.matches("[A-Z][1-9]") || inputLine.matches("[A-Z][1-9][0-9]"))) {
      throw new IllegalArgumentException(
          "Line must the: a capitalized letter between A-Z followed by a number between 1-99 ");
    }
  }

  /**
   * Checks if the <code>inputDestination</code> is a string with letters and spaces and no numbers.
   *
   * @param inputDestination is a String to describe the destination of the departure
   * @throws IllegalArgumentException if the input destination does not match the regex format
   */
  public static void validateDestination(String inputDestination) {
    if (!(inputDestination.matches("^[A-Za-zÆØÅæøå -]+$"))) {
      throw new IllegalArgumentException(
          "Destination must be a string with letters and spaces and no numbers");
    }
  }

  /**
   * Checks if the <code>inputTrack</code> is between 1 and 20 or -1.
   *
   * @param inputTrack is an int value representing the track where the departure,
   *                   departs the train station from
   * @throws IllegalArgumentException if the input track is not between 1 and 20 or -1
   */
  public static void validateTrackRange(int inputTrack) {
    if (inputTrack != -1 && (inputTrack < 1 || inputTrack > 20)) {
      throw new IllegalArgumentException("The track must be between 1 and 20 or -1");
    }
  }

  /**
   * Returns a String explaining that a train number isn't in the register.
   *
   * @return A String representing an error message
   */
  public static String invalidTrainNumberDescription() {
    return "You must type a train number that exists in the register";
  }
}
