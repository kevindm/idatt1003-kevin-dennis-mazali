package edu.ntnu.stud.model;

import edu.ntnu.stud.utils.ModelValidators;
import java.time.Duration;
import java.time.LocalTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Class describing a train departure register.
 *
 * <p>contains a constructor to create a departure register object with associated attributes. The
 * constructor verifies the input of the parameters entered, and throws exceptions on invalid data.
 *
 * <p>The class includes getters for all attributes, and setters for the attributes that are logical
 * to mutate. In this case, the current time of the day is the only one that is logical to mutate.
 *
 * <p>The object variables inside the register are mutable, but the reference of the register object
 * shouldn't be mutable. The Class also includes a toString method, which returns a string with the
 * information of all the departures within the register.
 *
 * <p>Goal: act as a model for the departure register object in the TrainDispatchApp.
 *
 * @author 111770
 * @version 1.0
 * @since 0.2
 */
public class TrainDepartureRegister {

  // the object variables are mutable because the user can change the time and manipulate the
  // arraylist
  private final ArrayList<Departure> theRegister;

  private LocalTime currentTime;

  /**
   * Constructs a TrainDepartureRegister by assigning the parameters below
   * as a part of my currentTime attribute.
   *
   * @param hours An int value representing the number of hours on the clock in HH:MM format.
   * @param min An int value representing the number of minutes on the clock in HH:MM format.
   */
  public TrainDepartureRegister(int hours, int min) {

    this.theRegister = new ArrayList<>();
    this.currentTime = LocalTime.of(hours, min);
  }

  /**
   * Checks if the train number is already in use: by checking an inputted integer.
   *
   * @param trainNumber is the unique number each departure gets when a departure is scheduled
   * @return null if the train number doesn't exist, otherwise the departure object with the
   *     inputted train number
   */
  private Departure searchForNumber(int trainNumber) {
    return theRegister.stream()
        .filter(departure -> departure.getTrainNumber() == trainNumber)
        .findAny()
        .orElse(null);
  }

  /**
   * Checks if the train number is already in use: by checking an inputted departure object.
   *
   * <p>through using the 'equals' method in the Departure class {@link Departure#equals(Object)}
   *
   * @param targetDeparture is the departure object that is being checked if it has an identical
   *     train number to another departure object
   * @return if the train number is already in use or not
   */
  private boolean isTrainNumberPresent(Departure targetDeparture) {
    return theRegister.contains(targetDeparture);
  }

  /**
   * Ads a new departure to the arraylist of departures.
   *
   * <p>If the requirements for the method are met:
   *
   * <ul>
   *   <li>The departure must be after the current time of day
   *   <li>The departure must be on a track not occupied by another departure within 5 minutes, if
   *       both departures are assigned
   *   <li>The departure must have a unique train number
   * </ul>
   *
   *<p>The method to check if the departure has a unique train number is {@link
   * #isTrainNumberPresent(Departure)}
   *
   * @param hours is an int value representing the number of hours of the clock in a HH:MM format
   * @param min is an int value the number of minutes of the clock in a HH:MM format
   * @param theLine is a String to describe which line the departure belongs to
   * @param theTrainNumber is an int value representing the unique number each departure gets when a
   *     departure is scheduled
   * @param theDestination is a String to describe the destination of the departure
   * @param theTrack is an int value representing the track where the departure, departs from the
   *     train station
   * @param theDelay is an int value representing the number of minutes a departure is delayed
   * @throws IllegalArgumentException if the aforementioned requirements aren't met
   */
  public void addNewDeparture(
      int hours,
      int min,
      String theLine,
      int theTrainNumber,
      String theDestination,
      int theTrack,
      int theDelay)
      throws IllegalArgumentException {

    // Creates a new departure object
    Departure departure =
        new Departure(hours, min, theLine, theTrainNumber, theDestination, theTrack, theDelay);

    // Creates a new local time object with the time of departure and the delay
    LocalTime timeOfDepartureWithDelay = LocalTime.of(hours, min).plusMinutes(theDelay);

    // Checks if the time of departure is within 5 minutes of another departure on the same track
    // except if the track is unassigned.
    // If the track is occupied by another departure are throw an exception
    for (Departure existingDeparture : theRegister) {
      if (existingDeparture.getTrack() == theTrack && existingDeparture.getTrack() != -1) {
        Duration duration =
            Duration.between(existingDeparture.getDepartureTime(), timeOfDepartureWithDelay);
        int minimumDepartureTime = 5;
        if (duration.toMinutes() < minimumDepartureTime) {
          throw new IllegalArgumentException(
              "The time between 2 departures on the same track must be at least 5 minutes");
        }
      }
    }

    // Checks if the train number is already in use and if the time of departure is after the
    // current time
    if (timeOfDepartureWithDelay.isAfter(currentTime) && !isTrainNumberPresent(departure)) {
      theRegister.add(departure);
    } else {
      throw new IllegalArgumentException(
          "The train number is already in use or the time of departure is before the current time");
    }
  }

  /**
   * Returns a deep-copy of the arraylist of departures. Utilizing the copy constructor of the
   * Departure class.
   *
   * @return the arraylist of departures - deep-copied
   */
  public ArrayList<Departure> deepCopy() {
    ArrayList<Departure> copyOfStation = new ArrayList<>();

    theRegister.forEach(copiedDeparture -> copyOfStation.add(new Departure(copiedDeparture)));
    return copyOfStation;
  }

  /**
   * Returns all the departures in the arraylist of departures sorted by time of departure
   * accounting for the possible delay.
   *
   * @return the sorted arraylist of departures.
   */
  public List<Departure> showSortedDepartures() {
    return deepCopy().stream()
            .sorted(Comparator.comparing(Departure::getDepartureTimeWithDelay))
            .collect(Collectors.toList());
  }

  /**
   * Shows a single departure in accordance with the train number inputted by the user.
   *
   * @param trainNumber is an int value representing the unique number each departure gets when a
   *     departure is scheduled
   * @return the departure object with the inputted train number
   */
  public Departure showDepartureTroughTrainNumber(int trainNumber) {
    return deepCopy().stream()
            .filter(departure -> departure.getTrainNumber() == trainNumber)
            .findFirst()
            .orElse(null);
  }
  /**
   * Shows one or several departures in accordance the destination inputted by the user.
   *
   * @param destination is the destination of the departure
   * @return an arraylist with the departure(s) with the inputted destination
   */

  public ArrayList<Departure> showDeparturesThroughDestination(String destination) {

    ArrayList<Departure> copyOfStation = new ArrayList<>();

    deepCopy().stream()
        .filter(departure -> Objects.equals(departure.getDestination(), destination))
        .forEach(departure -> copyOfStation.add(new Departure(departure)));

    return copyOfStation;
  }

  /**
   * Returns the current time of the day.
   *
   * @return a LocalTime Object representing the current time of the day.
   */
  public LocalTime getCurrentTime() {
    return currentTime;
  }

  /**
   * Sets the track of a departure in accordance with the train number inputted by the user. The
   * method changes the original departure object in the arraylist of departures. So that the deep
   * copy of the arraylist of departures is updated as well.
   *
   * @param trainNumber is an int value representing the unique number each departure gets when a
   *     departure is scheduled
   * @param track is an int value representing the track the train is assigned to
   */
  public void setTrackById(int trainNumber, int track) {
    ModelValidators.validateTrackRange(track);
    ModelValidators.validateTrainNumber(trainNumber);

    theRegister.stream()
        .filter(departure -> departure.getTrainNumber() == trainNumber)
        .forEach(departure -> departure.setTrack(track));
  }

  /**
   * Adds a delay to a departure in accordance, with the train number inputted by the user. {@link
   * #searchForNumber(int)} is used to check if the train number exists.
   *
   * @param trainNumber is an int value representing the unique number each train gets
   * @param delay is an int value the number of minutes a departure is delayed
   * @throws IllegalArgumentException if the train number doesn't exist or if the delay isn't
   *     between 1 and 60
   */
  public void setDelayTroughTrainNumber(int trainNumber, int delay)
      throws IllegalArgumentException {

    Departure theDeparture = searchForNumber(trainNumber);

    if (theDeparture == null) {
      throw new IllegalArgumentException(ModelValidators.invalidTrainNumberDescription());
    } else {
      theRegister.stream()
          .filter(departure -> departure.getTrainNumber() == trainNumber)
          .forEach(departure -> departure.setDelay(delay));
    }
  }

  /**
   * Assigns a departure to a specific track.
   *
   * <p>{@link #searchForNumber(int)} is used to check if the train number exists.
   *
   * @param trainNumber is an int value representing the unique number each train gets
   * @param track is an int value representing the track the train is assigned to
   * @throws IllegalArgumentException if the train number doesn't exist, or if the track isn't
   *     between 1 and 20 or is -1
   */
  public void assignTrackTroughTrainNumber(int trainNumber, int track)
      throws IllegalArgumentException {

    ModelValidators.validateTrackRange(track);
    Departure theDeparture = searchForNumber(trainNumber);

    if (theDeparture == null) {
      throw new IllegalArgumentException(ModelValidators.invalidTrainNumberDescription());
    } else {
      theRegister.stream()
          .filter(departure -> departure.getTrainNumber() == trainNumber)
          .forEach(departure -> departure.setTrack(track));
    }
  }

  /**
   * Removes a departure in accordance with the train number inputted by the user.
   *
   * <p>{@link #searchForNumber(int)} is used to check if the train number exists.
   *
   * @param trainNumber is the unique number each departure gets when a departure is scheduled
   * @throws IllegalArgumentException if the train number doesn't exist
   */
  public void removeDepartureThroughTrainNumber(int trainNumber) throws IllegalArgumentException {

    if (searchForNumber(trainNumber) != null) {
      theRegister.removeIf(departure -> departure.getTrainNumber() == trainNumber);

    } else {
      throw new IllegalArgumentException(ModelValidators.invalidTrainNumberDescription());
    }
  }

  /**
   * Set the current time of the day.
   *
   * <p>the requirements for the method are:
   *
   * <ul>
   *   <li>The hours must be between 0 and 23
   *   <li>The minutes must be between 0 and 59
   *   <li>The time must be after the current time of the day
   * </ul>
   *
   * @param hours is an int value representing the numbers of hours in HH
   * @param minutes is an int value representing the numbers of minutes in MM
   * @throws IllegalArgumentException if the requirements aren't met
   */
  public void setTheTime(int hours, int minutes) throws IllegalArgumentException {

    ModelValidators.validateHours(hours);
    ModelValidators.validateMinutes(minutes);

    if (this.currentTime.isBefore(LocalTime.of(hours, minutes))) {
      this.currentTime = LocalTime.of(hours, minutes);
    } else {
      throw new IllegalArgumentException("You can't go back in time");
    }
  }

  /**
   * Updates the departures in accordance with the current time of the day. So that the departures
   * who are scheduled before the current time of the day, are removed from the arraylist of
   * departures.
   */
  public void removeExpiredDepartures() {
    LocalTime currentTime = this.currentTime;

    theRegister.removeIf(
        departure -> departure.getDepartureTimeWithDelay().plusMinutes(-1).isBefore(currentTime));
  }

  /**
   * Returns a string with the information of all the departures within the register.
   *
   * @return departure information of the station
   */
  @Override
  public String toString() {

    List<Departure> sortedRegister = showSortedDepartures();
    return sortedRegister.stream().map(Departure::toString).collect(Collectors.joining("\n"));
  }

  /**
   * Returns the average delay of all the departures within the register.
   *
   * @return an int value representing the average delay of all the departures within the register
   * @throws IllegalArgumentException if the register is empty
   */
  public double averageDelay() throws IllegalArgumentException {
    if (deepCopy().isEmpty()) {
      throw new IllegalArgumentException(
          "The register is empty and therefore there is no average delay");
    }

    double totalDelay = deepCopy().stream().mapToDouble(Departure::getDelay).sum();

    return totalDelay / deepCopy().size();
  }

  /**
   * Returns the percentage of on time departures within the register.
   *
   * @return a double value representing the percentage of on time departures within the register
   * @throws IllegalArgumentException if the register is empty
   */
  public double onTimePercentage() throws IllegalArgumentException {
    if (deepCopy().isEmpty()) {
      throw new IllegalArgumentException(
          "The register is empty and therefore there is no on time percentage");
    }

    long onTimeDepartures =
        deepCopy().stream().filter(departure -> departure.getDelay() == 0).count();

    return (double) onTimeDepartures / deepCopy().size() * 100;
  }

  /**
   * Returns the percentage of delayed departures within the register.
   *
   * @return a double value representing the percentage of delayed departures within the register
   * @throws IllegalArgumentException if the register is empty
   */
  public List<String> mostPopularDestinations() {
    if (deepCopy().isEmpty()) {
      throw new IllegalArgumentException(
          "The register is empty, and therefore, there are no most popular destinations");
    }

    Map<String, Long> destinationCount =
        deepCopy().stream()
            .collect(Collectors.groupingBy(Departure::getDestination, Collectors.counting()));

    long maxCount = destinationCount.values().stream().max(Long::compare).orElse(0L);

    List<String> mostPopularDestinations =
        destinationCount.entrySet().stream()
            .filter(entry -> entry.getValue() == maxCount)
            .map(Map.Entry::getKey)
            .collect(Collectors.toList());

    if (mostPopularDestinations.isEmpty()) {
      throw new IllegalArgumentException("No popular destination found");
    }

    return mostPopularDestinations;
  }

  /**
   * Returns the most popular destinations within the register.
   *
   * @return a list of strings representing the most popular destinations within the register
   * @throws IllegalArgumentException if the register is empty
   */
  public List<Departure> getMostDelayedDepartures() throws IllegalArgumentException {
    if (deepCopy().isEmpty()) {
      throw new IllegalArgumentException(
          "The register is empty, and therefore, there are no most delayed departures ");
    }

    OptionalInt maxDelay = deepCopy().stream().mapToInt(Departure::getDelay).max();

    return deepCopy().stream()
        .filter(departure -> departure.getDelay() == maxDelay.orElse(0))
        .collect(Collectors.toList());
  }
}
