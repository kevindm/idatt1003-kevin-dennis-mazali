package edu.ntnu.stud.model;

import edu.ntnu.stud.utils.ModelValidators;
import java.time.LocalTime;
import java.util.Objects;

/**
 * Class describing a train departure.
 *
 * <p>contains a constructor to create a departure object with associated attributes. The
 * constructor verifies the input of the parameters entered, and throws exceptions on invalid data.
 *
 * <p>The class includes getters for all attributes, and setters for the attributes that are logical
 * to mutate. In this case, the track and delay attributes are the only ones that are logical to
 * mutate. The class also includes a toString method, which returns a string with the information of
 * the departure, depending on the values of the attributes.
 *
 * <p>Goal: act as a model for the departure objects in the TrainDispatchApp.
 *
 * @author 111770
 * @version 1.0
 * @since 0.1
 */
public class Departure {

  private final LocalTime departureTime;

  private final String line;

  private final int trainNumber;

  private final String destination;

  private int track;

  private int delay;

  /**
   * Constructs a Departure by assigning the parameters below to the attributes of the Departure.
   *
   * <p>Does this only after verifying that the parameters are valid, by calling the validation
   * methods in the ModelValidators class. The validation methods are:
   *
   * <ol>
   *   <li>{@link ModelValidators#validateMinutes(int)}
   *   <li>{@link ModelValidators#validateHours(int)}
   *   <li>{@link ModelValidators#validateTrainNumber(int)}
   *   <li>{@link ModelValidators#validateDelayRange(int)}
   *   <li>{@link ModelValidators#validateLine(String)}
   *   <li>{@link ModelValidators#validateDestination(String)}
   *   <li>{@link ModelValidators#validateTrackRange(int)}
   * </ol>
   *
   * @param inputHours is an int value representing the number of hours of the clock in a HH:MM
   *     format
   * @param inputMin is an int value the number of minutes of the clock in a HH:MM format
   * @param inputLine is a String to describe which line the departure belongs to
   * @param inputTrainNumber is an int value representing the unique number each departure gets when
   *     a departure is scheduled
   * @param inputDestination is a String to describe the destination of the departure
   * @param inputTrack is an int value representing the track where the departure, departs from the
   *     train station
   * @param inputDelay is an int value representing the number of minutes a departure is delayed -
   *     the inspiration for the usage of this parameter is from the usage og the AI-tool
   *     ChatGPT-3.5
   * @throws IllegalArgumentException if the String inputs do not match the regex format, or if the
   *     int inputs are not within the specified range
   */
  public Departure(
      int inputHours,
      int inputMin,
      String inputLine,
      int inputTrainNumber,
      String inputDestination,
      int inputTrack,
      int inputDelay)
      throws IllegalArgumentException {

    ModelValidators.validateHours(inputHours);
    ModelValidators.validateMinutes(inputMin);
    ModelValidators.validateTrainNumber(inputTrainNumber);
    ModelValidators.validateDelayRange(inputDelay);
    ModelValidators.validateLine(inputLine);
    ModelValidators.validateDestination(inputDestination);
    ModelValidators.validateTrackRange(inputTrack);

    this.departureTime = LocalTime.of(inputHours, inputMin);
    this.line = inputLine;
    this.trainNumber = inputTrainNumber;
    this.destination = inputDestination;
    this.delay = inputDelay;
    this.track = inputTrack;
  }

  /**
   * Deep-copy constructor to create a deep copy of a departure object. This deep copy is returned
   * as a new departure object, the original departure object will not be altered.
   *
   * @param departure is the departure which is deep-copied into the "new" departure object created
   *     by the constructor
   */
  public Departure(Departure departure) {
    this.departureTime = departure.departureTime;
    this.line = departure.line;
    this.trainNumber = departure.trainNumber;
    this.destination = departure.destination;
    this.delay = departure.delay;
    this.track = departure.track;
  }

  /**
   * Returns the LocalTime object representing the time of departure.
   *
   * @return the time of departure
   */
  public LocalTime getDepartureTime() {
    return departureTime;
  }

  /**
   * Returns the LocalTime object representing the time of departure, with the delay added to it.
   *
   * @return the time of departure with the delay added to it
   */
  public LocalTime getDepartureTimeWithDelay() {
    return departureTime.plusMinutes(delay);
  }

  /**
   * Returns a String representing the line of the departure.
   *
   * @return the line of the departure
   */
  public String getLine() {
    return line;
  }

  /**
   * Returns an int value representing the train number of the departure.
   *
   * @return the train number of the departure
   */
  public int getTrainNumber() {
    return trainNumber;
  }

  /**
   * Returns a String representing the destination of the departure.
   *
   * @return the destination of the departure
   */
  public String getDestination() {
    return destination;
  }

  /**
   * Returns an int value representing the track of the departure.
   *
   * @return the track of the departure
   */
  public int getTrack() {
    return this.track;
  }

  /**
   * Returns an int value representing the delay of the departure.
   *
   * @return the delay of the departure
   */
  public int getDelay() {
    return delay;
  }

  /**
   * Sets an int as the track of the departure of this existing departure object. This allows for
   * assigning the departure to a specific track.
   *
   * @param inputTrack is an int representing the track the train is assigned to
   * @throws IllegalArgumentException if the track number is not between 1 and 20 (maximum number of
   *     tracks in the station) or is -1 (value representing the departure not being assigned to a
   *     track)
   */
  public void setTrack(int inputTrack) throws IllegalArgumentException {

    if (this.track == inputTrack) {
      throw new IllegalArgumentException(
          "You can't set the same track as the departure is already assigned to");
    }

    ModelValidators.validateTrackRange(inputTrack);
    this.track = inputTrack;
  }

  /**
   * Sets an int as the delay of the departure of this existing departure object. This allows for
   * assigning a delay to the departure.
   *
   * @param inputDelay is an int representing the delay of the departure
   * @throws IllegalArgumentException if the delay is not between 1 and 60 (maximum delay of a
   *     departure) or is the same as the delay already assigned to the departure
   */
  public void setDelay(int inputDelay) throws IllegalArgumentException {

    if (this.delay == inputDelay) {
      throw new IllegalArgumentException(
          "You can't set the same delay as the departure is already assigned to");
    }
    ModelValidators.validateDelayRange(inputDelay);

    this.delay = inputDelay;
  }

  /**
   * Checks if another departure is considered the same as this object. The method returns true if
   * the train number of the departure is the same as the train number of this other object.
   *
   * @param other is the other departure object compared to this object
   * @return true if the train number of the departure is the same as the train number of this
   *     object, otherwise false
   */
  @Override
  public boolean equals(Object other) {
    if (this == other) {
      return true;
    }
    if (other == null || getClass() != other.getClass()) {
      return false;
    }
    Departure departure = (Departure) other;
    return trainNumber == departure.trainNumber;
  }

  /**
   * Returns a hash code value for the object based on its uniqueness.
   *
   * @return a unique integer hash code that represents the item
   */
  @Override
  public int hashCode() {
    return Objects.hash(trainNumber);
  }

  /**
   * Returns a String that represents the information of a departure. Depending on the arguments of
   * the departure object, with three different formats:
   *
   * <ol>
   *   <li>when the departure is assigned to a track and has a delay.
   *   <li>When the departure is assigned to a track and has no delay.
   *   <li>When the departure is not assigned to a track.
   * </ol>
   *
   * @return the information of a departure
   */
  @Override
  public String toString() {

    String blankSpace = "";

    LocalTime delayedTime = departureTime.plusMinutes(delay);
    String departureText =
        (delay > 0)
            ? String.format("%02d:%02d", delayedTime.getHour(), delayedTime.getMinute())
            : "";

    if (track != -1) {
      return String.format(
          "|%02d:%02d %13s|  %-4s|  %-12s|  %-28s| %-22s | %5d | ",
          departureTime.getHour(),
          departureTime.getMinute(),
          blankSpace,
          getLine(),
          getTrainNumber(),
          getDestination(),
          departureText,
          getTrack());
    } else {
      return String.format(
              "|%02d:%02d %13s|  %-4s|  %-12s|  %-28s| %-22s | %5s | ",
              departureTime.getHour(),
              departureTime.getMinute(),
              blankSpace,
              getLine(),
              getTrainNumber(),
              getDestination(),
              departureText,
              blankSpace);


    }
  }
}
