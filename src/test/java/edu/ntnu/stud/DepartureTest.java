package edu.ntnu.stud;

import static org.junit.jupiter.api.Assertions.*;

import edu.ntnu.stud.model.TrainDepartureRegister;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

/**
 * Class containing tests for the Departure class.
 *
 * <p>the class was created with the help of GitHub Copilot, when it comes to the repetitive code
 * structure of test methods. By the implementation of the AAA pattern - Assert-Act-Arrange and the
 * naming of the test methods.
 */
class DepartureTest {

  private TrainDepartureRegister OsloTest;

  @BeforeEach
  void setup() {

    OsloTest = new TrainDepartureRegister(15, 20);

    OsloTest.addNewDeparture(15, 20, "L2", 11, "Bergen", 2, 3);
  }

  @Nested
  @DisplayName("Positive Tests - Get Tests")
  class GetTests {

    @Test
    @DisplayName("Departure  - Get Train Number")
    void getTrainNumber() {

      int trainNumber = OsloTest.deepCopy().get(0).getTrainNumber();
      assertEquals(11, trainNumber);
    }

    @Test
    @DisplayName("Departure - Get Departure Time")
    void getTimeOfDeparture() {

      String departureTime = OsloTest.deepCopy().get(0).getDepartureTime().toString();
      assertEquals("15:20", departureTime);
    }

    @Test
    @DisplayName("Departure - Get Delay")
    void getDelay() {

      int delay = OsloTest.deepCopy().get(0).getDelay();
      assertEquals(3, delay);
    }

    @Test
    @DisplayName("Departure - Get Destination")
    void getDestination() {

      String destination = OsloTest.deepCopy().get(0).getDestination();
      assertEquals("Bergen", destination);
    }

    @Test
    @DisplayName("Departure - Get Departure Time with Delay")
    void getDepartureTimeWithDelay() {

      String timeOfDepartureWithDelay =
          OsloTest.deepCopy().get(0).getDepartureTimeWithDelay().toString();
      assertEquals("15:23", timeOfDepartureWithDelay);
    }

    @Test
    @DisplayName("Departure - Get Track")
    void getTrack() {

      int track = OsloTest.deepCopy().get(0).getTrack();
      assertEquals(2, track);
    }
  }

  @Nested
  @DisplayName("Positive Tests - Void Test and toString Test")
  class SetAndToString {

    @Test
    @DisplayName("Departure - Set a new delay")
    void setTrack() {
      int newTrack = 4;
      OsloTest.setTrackById(11, newTrack);
      assertEquals(OsloTest.deepCopy().get(0).getTrack(), newTrack);
    }

    @Test
    @DisplayName("Departure - correct string format")
    void testToString() {

      String toString = OsloTest.deepCopy().get(0).toString();

      assertEquals(
          "|15:20              |  L2  |  11          |  Bergen                      | 15:23                  |     2 | ",
          toString);
    }
  }

  @Nested
  @DisplayName(
      "Negative tests for 'Departure' methods, throws IllegalArgumentExceptions on wrong input parameters")
  class ConstructorTests {

    @Test
    @DisplayName("Departure void method throws 'Ill.Arg.Exc.' on a non changed delay value ")
    void sameDelayAsBefore() {

      int originalDelay = OsloTest.deepCopy().get(0).getDelay();

      assertThrows(
          IllegalArgumentException.class, () -> OsloTest.deepCopy().get(0).setDelay(originalDelay));
    }

    @Test
    @DisplayName("Departure void method throws 'Ill.Arg.Exc' on a non changed track value ")
    void sameTrackAsBefore() {

      int originalTrack = OsloTest.deepCopy().get(0).getTrack();

      assertThrows(
          IllegalArgumentException.class, () -> OsloTest.deepCopy().get(0).setTrack(originalTrack));
    }

    @Test
    @DisplayName("Departure constructor throws 'Ill.Arg.Exc.' on invalid track range")
    void invalidTrackInConstructor() {
      assertThrows(
          IllegalArgumentException.class,
          () -> OsloTest.addNewDeparture(20, 26, "L7", 14, "Bergen", -5, 0));
    }

    @Test
    @DisplayName("Departure constructor throws 'Ill.Arg.Exc.' on invalid delay range")
    void invalidDelayInConstructor() {
      assertThrows(
          IllegalArgumentException.class,
          () -> OsloTest.addNewDeparture(20, 26, "L7", 15, "Bergen", 5, -7));
    }

    @Test
    @DisplayName("Departure constructor throws 'Ill.Arg.Exc.' on invalid train number range")
    void invalidTrainNumberInConstructor() {
      assertThrows(
          IllegalArgumentException.class,
          () -> OsloTest.addNewDeparture(20, 26, "L7", -15, "Bergen", 2, 4));
    }

    @Test
    @DisplayName("Departure constructor throws 'Ill.Arg.Exc.' on invalid hour range")
    void invalidHourInConstructor() {

      assertThrows(
          IllegalArgumentException.class,
          () -> OsloTest.addNewDeparture(27, 26, "L7", 15, "Bergen", 7, 5));
    }

    @Test
    @DisplayName("Departure constructor throws 'Ill.Arg.Exc.' on invalid minute range")
    void invalidMinuteInConstructor() {

      assertThrows(
          IllegalArgumentException.class,
          () -> OsloTest.addNewDeparture(20, 61, "L7", 16, "Bergen", 7, 5));
    }

    @Test
    @DisplayName("Departure constructor throws 'Ill.Arg.Exc.' on invalid line format")
    void invalidLineFormat() {
      // correct format regex: "[A-Z][0-9]") or "[A-Z][0-9][0-9]"
      assertThrows(
          IllegalArgumentException.class,
          () -> OsloTest.addNewDeparture(20, 26, "L", 15, "Bergen", 2, 0));
    }

    @Test
    @DisplayName("Departure constructor throws 'Ill.Arg.Exc.' on invalid destination format")
    void invalidDestinationFormat() {
      // correct format regex: "^[A-Za-zÆØÅæøå -]+$"
      assertThrows(
          IllegalArgumentException.class,
          () -> OsloTest.addNewDeparture(20, 26, "L7", 15, "Bergen 5 5", 2, 0));
    }
  }
}
