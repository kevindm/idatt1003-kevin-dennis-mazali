package edu.ntnu.stud;

import edu.ntnu.stud.model.Departure;
import edu.ntnu.stud.model.TrainDepartureRegister;
import org.junit.jupiter.api.*;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Class containing tests for the TrainDepartureRegister class.
 *
 * <p>the class was created with the help of GitHub Copilot, when it comes to the repetitive code
 * structure of test methods. By the implementation of the AAA pattern - Assert-Act-Arrange and the
 * naming of the test methods.
 */
class RegisterTest {

  private final int FIRST_DEPARTURE_INDEX = 0;

  private TrainDepartureRegister OsloTest;

  @BeforeEach
  void setup() {

    // hours must be within 0-23
    // minutes must be within 0-59
    OsloTest = new TrainDepartureRegister(15, 20);

    // adding departure objects to the arraylist of theStation to test the TrainDepartureRegister

    OsloTest.addNewDeparture(17, 20, "L2", 2, "Bergen", 2, 0);

    OsloTest.addNewDeparture(16, 20, "L4", 3, "Oslo", 5, 0);
  }

  @Nested
  @DisplayName("Positive Tests - Get Tests")
  class GetTests {

    @Nested
    @DisplayName("Register - Main Module")
    class MainModule {

      @Test
      @DisplayName("Register - Get the current time")
      void getCurrentTime() {

        String currentTime = "15:20";
        assertEquals(OsloTest.getCurrentTime().toString(), currentTime);
      }

      @Test
      @DisplayName("Register - Show Departures chronologically")
      void showDeparturesChronologically() {

        TrainDepartureRegister correctOrder = new TrainDepartureRegister(15, 20);

        correctOrder.addNewDeparture(16, 20, "L4", 3, "Oslo", 5, 0);
        correctOrder.addNewDeparture(17, 20, "L2", 2, "Bergen", 2, 0);

        List<Departure> sortedDepartures = OsloTest.showSortedDepartures();

        assertEquals(sortedDepartures, correctOrder.deepCopy());
      }

      @Test
      @DisplayName("Register - Show Departures Through Destination")
      void showDeparturesThroughDestination() {

        String destination = "Bergen";
        ArrayList<Departure> copyOfStation = new ArrayList<>();

        for (Departure departure : OsloTest.deepCopy()) {
          if (Objects.equals(departure.getDestination(), destination)) {
            copyOfStation.add(departure);
          }
        }

        assertEquals(OsloTest.showDeparturesThroughDestination(destination), copyOfStation);
      }

      @Test
      @DisplayName("Register - Show Departure Through Train Number")
      void showDepartureThroughTrainNumber() {
        int trainNumber = 2;
        Departure result = OsloTest.showDepartureTroughTrainNumber(trainNumber);

        assertEquals(OsloTest.deepCopy().get(0).toString(), result.toString());
      }

      @Nested
      @DisplayName("Register - Statistics Module")
      class StatisticsModule {

        @Test
        @DisplayName("Register - Get the average delay of the departures")
        void getAverageDelay() {

          int averageDelay = 0;
          for (Departure departure : OsloTest.deepCopy()) {
            averageDelay += departure.getDelay();
          }
          averageDelay = averageDelay / OsloTest.deepCopy().size();

          assertEquals(OsloTest.averageDelay(), averageDelay);
        }

        @Test
        @DisplayName("Register - On time percentage")
        void onTimePercentage() {

          double onTimePercentage = 0;
          for (Departure departure : OsloTest.deepCopy()) {
            if (departure.getDelay() == 0) {
              onTimePercentage += 1;
            }
          }
          onTimePercentage = (onTimePercentage * 100) / OsloTest.deepCopy().size();

          assertEquals(OsloTest.onTimePercentage(), onTimePercentage);
        }

        @Test
        @DisplayName("Register - Most popular destinations")
        void mostPopularDestination() {

          // adding a new departure object to the arraylist of theStation to make oslo the most
          // popular destination
          OsloTest.addNewDeparture(16, 50, "L4", 39, "Oslo", 5, 45);

          ArrayList<String> destinations = new ArrayList<>();
          ArrayList<String> mostPopularDestinations = new ArrayList<>();

          for (Departure departure : OsloTest.deepCopy()) {
            destinations.add(departure.getDestination());
          }

          // adding the most popular destination to the arraylist of mostPopularDestinations
          for (String destination : destinations) {
            if (Objects.equals(destination, "Oslo")
                && !mostPopularDestinations.contains(destination)) {
              mostPopularDestinations.add(destination);
            }
          }

          assertEquals(OsloTest.mostPopularDestinations(), mostPopularDestinations);
        }

        @Test
        @DisplayName("Register - Most delayed departures")
        void mostDelayedDepartures() {

          // adding to new departure objects to the arraylist of theStation to make them the most
          // delayed departures
          OsloTest.addNewDeparture(16, 50, "L4", 39, "Oslo", 5, 45);
          OsloTest.addNewDeparture(16, 20, "L4", 79, "Oslo", 19, 45);

          ArrayList<Departure> mostDelayedDepartures = new ArrayList<>();

          ArrayList<Departure> departures = new ArrayList<>(OsloTest.deepCopy());

          // adding the most delayed departures to the arraylist of mostDelayedDepartures
          for (Departure departure : departures) {
            if (departure.getDelay() == 45) {
              mostDelayedDepartures.add(departure);
            }
          }

          assertEquals(OsloTest.getMostDelayedDepartures(), mostDelayedDepartures);
        }
      }
    }
  }

  @Nested
  @DisplayName("Positive Tests - Void Tests")
  class VoidTests {

    @Test
    @DisplayName("Register - Add Departure")
    void addNewDeparture() {

      // current size of the arraylist of theStation before adding a new departure object
      int formerSize = OsloTest.deepCopy().size();

      // adding a new departure object to the arraylist of theStation
      OsloTest.addNewDeparture(15, 43, "L2", 25, "Bergen", 9, 3);
      int currentSize = OsloTest.deepCopy().size();

      // checking if the size of the arraylist of theStation has increased by 1
      assertEquals(currentSize - 1, formerSize);
    }

    @Test
    @DisplayName("Register - Remove Departure Through Train Number")
    void removeDepartureThroughTrainNumber() {

      int trainNumber = OsloTest.deepCopy().get(FIRST_DEPARTURE_INDEX).getTrainNumber();

      // current size of the arraylist of theStation before removing the departure object
      int formerSize = OsloTest.deepCopy().size();

      // removing the departure object with the train number of the first departure object
      OsloTest.removeDepartureThroughTrainNumber(trainNumber);
      int currentSize = OsloTest.deepCopy().size();

      // checking if the size of the arraylist of theStation has decreased by 1
      assertEquals(currentSize + 1, formerSize);
    }

    @Test
    @DisplayName("Register - Remove Expired Departures")
    void removeExpiredDepartures() {

      TrainDepartureRegister OsloTestExpire = new TrainDepartureRegister(15, 20);

      OsloTestExpire.addNewDeparture(15, 25, "L2", 8, "Bergen", 2, 3);
      // current size of the arraylist of theStation before removing the expired departures
      int formerSize = OsloTestExpire.deepCopy().size();

      // setting the current time after the departure time of the departure object
      OsloTestExpire.setTheTime(15, 30);
      // removing the expired departures
      OsloTestExpire.removeExpiredDepartures();

      // current size of the arraylist of theStation after removing the expired departures
      int currentSize = OsloTestExpire.deepCopy().size();

      // checking if the size of the arraylist of theStation has decreased by 1, because there was
      // only 1 departure object that was expired
      assertEquals(currentSize + 1, formerSize);
    }

    @Test
    @DisplayName("Register - Add Delay Through A train Number")
    void addDelay() {

      int trainNumber = 2;
      int newDelay = 5;

      OsloTest.setDelayTroughTrainNumber(trainNumber, newDelay);

      assertEquals(newDelay, OsloTest.deepCopy().get(FIRST_DEPARTURE_INDEX).getDelay());
    }

    @Test
    @DisplayName("Register - Set Track Through Train Number")
    void setTrack() {

      int trainNumber = OsloTest.deepCopy().get(FIRST_DEPARTURE_INDEX).getTrainNumber();
      int newTrack = 5;

      OsloTest.assignTrackTroughTrainNumber(trainNumber, newTrack);

      assertEquals(OsloTest.deepCopy().get(FIRST_DEPARTURE_INDEX).getTrack(), newTrack);
    }

    @Test
    @DisplayName("Register - Show departures through a destination")
    void showDeparturesThroughADestination() {

      String destination = "Bergen";
      int numberOfBergenDepartures = 0;
      ArrayList<Departure> copyOfStation = new ArrayList<>();

      for (Departure departure : OsloTest.deepCopy()) {
        if (Objects.equals(departure.getDestination(), destination)) {
          copyOfStation.add(departure);
          numberOfBergenDepartures += 1;
        }
      }

      assert (copyOfStation.size() == numberOfBergenDepartures);
    }

    @Test
    @DisplayName("Register - Set the current Time")
    void setTheTime() {

      LocalTime newTime = LocalTime.of(16, 20);

      OsloTest.setTheTime(newTime.getHour(), newTime.getMinute());
      assertEquals(OsloTest.getCurrentTime(), newTime);
    }
  }

  @Nested
  @DisplayName("Negative Tests for 'Register' methods - IllegalArgumentException")
  class methodTests {

    @Test
    @DisplayName(
        "Register void method throws 'Ill.Arg.Exc.' on insufficient time between departures on the same track")
    void insufficientTimeBetweenDepartures() {

      // creating a new departure object with a time of departure that is 3 minutes after the time
      // of departure of the first departure object on the same Track
      LocalTime InsufficientTime =
          OsloTest.deepCopy().get(FIRST_DEPARTURE_INDEX).getDepartureTime().plusMinutes(3);
      int sameTrack = OsloTest.deepCopy().get(FIRST_DEPARTURE_INDEX).getTrack();

      Departure newDeparture =
          new Departure(
              InsufficientTime.getHour(),
              InsufficientTime.getMinute(),
              "L7",
              17,
              "Bergen",
              sameTrack,
              0);

      assertThrows(
          IllegalArgumentException.class,
          () ->
              OsloTest.addNewDeparture(
                  newDeparture.getDepartureTime().getHour(),
                  newDeparture.getDepartureTime().getMinute(),
                  newDeparture.getLine(),
                  newDeparture.getTrainNumber(),
                  newDeparture.getDestination(),
                  newDeparture.getTrack(),
                  newDeparture.getDelay()));
    }

    @Test
    @DisplayName(
        "Register void method throws 'Ill.Arg.Exc.' on invalid Train Number, uses an already existing train number")
    void nonUniqueTrainNumberInNewDeparture() {

      // creating a new departure object with a train number that already exists
      int sameTrainNumber = OsloTest.deepCopy().get(FIRST_DEPARTURE_INDEX).getTrainNumber();

      Departure newDeparture = new Departure(20, 26, "L7", sameTrainNumber, "Bergen", 2, 4);
      assertThrows(
          IllegalArgumentException.class,
          () ->
              OsloTest.addNewDeparture(
                  newDeparture.getDepartureTime().getHour(),
                  newDeparture.getDepartureTime().getMinute(),
                  newDeparture.getLine(),
                  newDeparture.getTrainNumber(),
                  newDeparture.getDestination(),
                  newDeparture.getTrack(),
                  newDeparture.getDelay()));
    }

    @Test
    @DisplayName(
        "Register void method throws 'Ill.Arg.Exc.' on invalid departure Time: time is before current time")
    void InvalidDepartureTime() {

      // creating a new departure object with a time of departure that is earlier than the internal
      // clock
      int oneHourEarlier = OsloTest.getCurrentTime().getHour() - 1;
      int sameMinute = OsloTest.getCurrentTime().getMinute();

      Departure newDeparture = new Departure(oneHourEarlier, sameMinute, "L7", 239, "Bergen", 2, 0);
      assertThrows(
          IllegalArgumentException.class,
          () ->
              OsloTest.addNewDeparture(
                  newDeparture.getDepartureTime().getHour(),
                  newDeparture.getDepartureTime().getMinute(),
                  newDeparture.getLine(),
                  newDeparture.getTrainNumber(),
                  newDeparture.getDestination(),
                  newDeparture.getTrack(),
                  newDeparture.getDelay()));
    }

    @Test
    @DisplayName(
        "Register void method throws 'Ill.Arg.Exc' on invalid train number and invalid delay")
    void setNegativeDelayValue() {

      int trainNumber = OsloTest.deepCopy().get(FIRST_DEPARTURE_INDEX).getTrainNumber();

      assertThrows(
          IllegalArgumentException.class,
          () -> OsloTest.setDelayTroughTrainNumber(-trainNumber, 2));
      assertThrows(
          IllegalArgumentException.class,
          () -> OsloTest.setDelayTroughTrainNumber(trainNumber, -2));
    }

    @Test
    @DisplayName(
        "Register void method throws 'Ill.Arg.Exc' on invalid current time of day: can't go back in time")
    void invalidCantGoBackInTime() {
      // the current time is 15:20

      LocalTime newTime = LocalTime.of(14, 20);
      assertThrows(
          IllegalArgumentException.class,
          () -> OsloTest.setTheTime(newTime.getHour(), newTime.getMinute()));
    }
  }
}
