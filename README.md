# Portfolio project IDATT1003 - 2023

### STUDENT NAME = "Kevin Dennis Mazali"  
### STUDENT ID = "111770"

## Project description

The project is a simplified Train Dispatch App.
The intended target group for this project is a train operations manager
who is in charge of all departures from a single station.

### Functionality of the program:
 - View the departures sorted by departure time.
 - Add or remove departures.
 - View a departure's details.
 - Change a departure's details.
 - Set the current 
 - View statistics about the departures.

### Project Boundaries

- The system only handles departures from a single station
- The departures are only within a single day
- The current time needs to be updated manually, and the time cant be set backwards in time.

## Project structure

[//]: # (TODO: Describe the structure of your project here. How have you used packages in your structure. Where are all sourcefiles stored. Where are all JUnit-test classes stored. etc.)

The Project is divided in the 2 main directories 'main' & 'test' they respectively contain the program and the testing for the program.
the test directory is where the JUnit-test classes are stored while the different packages that make up the functionality of the project
are within the 'main' directory. These include; 'launch,' 'model,' 'ui,' 'utils'

## Link to repository

[//]: # (TODO: Include a link to your repository here.)
https://gitlab.stud.idi.ntnu.no/kevindm/idatt1003-kevin-dennis-mazali

## How to run the project

[//]: # (TODO: Describe how to run your project here. What is the main class? What is the main method?
What is the input and output of the program? What is the expected behaviour of the program?)

The main class of the application is within the 'launch' package that contains the 'TrainDispatchApp' class.
This class contains the UI the init and start methods that contains the UI. Running the main method of this class
starts the application.

## How to run the tests

[//]: # (TODO: Describe how to run the tests here.)

The tests are run by running the respective test classes for each model class of the project;
Departure & TrainDepartureRegister.
DepartureTest & TrainDepartureRegisterTest respectively.

## References

[//]: # (TODO: Include references here, if any. For example, if you have used code from the course book, include a reference to the chapter.
Or if you have used code from a website or other source, include a link to the source.)
The project has been developed by the student. Github Copilot has been used to generate parts of the code, this is clearly stated through Javadoc comments.